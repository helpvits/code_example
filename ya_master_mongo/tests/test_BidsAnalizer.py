from master.BidsAnalyzer import *
from tests.tests_data.bids_analizer_data import *


def test_get_bids_avg():
    assert get_bids_avg(get_bids_valid_data) == get_bids_valid_result
    assert get_bids_avg(get_bids_one_empty_data) == get_bids_one_empty_result
    assert get_bids_avg(get_bids_one_valid_data) == get_bids_one_valid_result
    assert get_bids_avg(get_bids_empty_data) == get_bids_empty_result


def test_get_prices_dict():
    assert get_prices_dict(get_bids_valid_data) == get_prices_dict_valid_data
    assert get_prices_dict(get_bids_one_empty_data) == get_prices_dict_one_empty_data
    assert get_prices_dict(get_bids_one_valid_data) == get_prices_dict_one_valid_data
    assert get_prices_dict(get_bids_empty_data) == get_prices_dict_empty_data
