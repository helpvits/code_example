from GenerateRec import data_slicer


def test_data_slicer():
    test_slice = ({'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 0}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 0}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 1}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 1}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 2}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 2}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 3}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 3}},
                  )

    empty_data = ()
    three_data = ({'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 0}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 0}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 1}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 1}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 2}},
                  {'TimeStamp': {'WD': 2, 'D': 8, 'M': 6, 'Y': 2021, 'h': 17, 'm': 18, 's': 25, 'TS': 2}})

    assert data_slicer(test_slice, -1) == empty_data
    assert data_slicer(test_slice, 0) == empty_data
    assert data_slicer(test_slice, 3) == three_data
    assert data_slicer(test_slice, 5) == test_slice
