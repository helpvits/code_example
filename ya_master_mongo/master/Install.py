import pymongo
from pymongo import MongoClient
from DATA.config import mongo_path, accounts
from master.API import API
from master.DataBase import Database


class InstallData:

    def __init__(self, db_path=mongo_path):
        client = MongoClient(db_path)                       # Монга далеко
        self.db = client["direct"]                          # Прямой доступ к БД
        self.db_manager = Database()                        # Доступ к моим функциям

    # ============ Очистка базы ==============
    def clear_db(self):
        self.clear_keyword_bids()
        self.clear_keywords()
        self.clear_groups()
        self.clear_campaigns()
        self.clear_accounts()
        self.clear_keywords_info()

    def clear_accounts(self):
        try:
            self.db_manager.db.Accounts.drop()
        except Exception as e:
            print(e)
            pass

    def clear_campaigns(self):
        try:
            self.db_manager.db.Campaigns.drop()
        except Exception as e:
            print(e)
            pass

    def clear_groups(self):
        try:
            self.db_manager.db.AdGroups.drop()
        except Exception as e:
            print(e)
            pass

    def clear_keyword_bids(self):
        try:
            self.db_manager.db.KeywordBids.drop()
        except Exception as e:
            print(e)
            pass

    def clear_keywords(self):
        try:
            self.db_manager.db.Keywords.drop()
        except Exception as e:
            print(e)
            pass

    def clear_keywords_info(self):
        try:
            self.db_manager.db.KeywordsInfo.drop()
        except Exception as e:
            print(e)
            pass

    # =========== Заполнение базы ==========
    # Добавляет акаунт в базу
    # TODO Not Used
    def add_account(self, account: dict):
        """
        :param account:
            {
                "_id": 1,
                "Account": "наззвание",
                "Token": "AAAAA-AAAAA-asfasfsadfsadf",
                "Login": "Логин акаунта"
            }
        :return:
        """
        try:
            self.db_manager.db.Accounts.insert_one(account)                    # Попытка добавить данные в базу
        except pymongo.errors.DuplicateKeyError:                    # Если данные уже есть:
            self.db_manager.db.Accounts.update_one({"_id": account["_id"]},    # Обновляем данные по id
                                        {"$set": account})  # {"Id": i["Id"]} присвоение айдишника базы из айди обтекта
        except Exception as ex:
            print("Возникла ошибка при добавлении аккаунта")
            print(ex)

    # Заносит акаунты из конфига в базу
    def add_accounts(self):
        self.db_manager.db.Accounts.insert_many(accounts)

    # ======= INSTALL =======
    # Чистая база с акаунтами
    def install(self):
        self.clear_db()
        self.add_accounts()

    # Заполнение базы

    # Заполняем компании
    def get_all_campaigns(self):
        for account in self.db_manager.get_accounts():
            api = API(account)
            campaigns = api.campaigns_get()
            try:
                self.db_manager.data_to_base('Campaigns', campaigns.json()["result"]["Campaigns"], account,
                                             strategy="Min")
            except KeyError:
                print('При заполненнии списка компаний, яндекс не прислал нужные значения для компании',
                      account)
                pass
            except AttributeError:
                print('При заполненнии списка компаний, яндекс не прислал нужные значения для компании',
                      account, ' Возможно просрочен токен')
                pass

    # Группы
    def get_all_groups(self):
        for account in self.db_manager.get_accounts():
            api = API(account)
            campaigns = self.db_manager.get_campaigns_by_account(account)
            for campaign in campaigns:
                ad_groups = api.ad_groups_get(type_criteria="CampaignIds", criteria_data=[campaign["_id"]])
                try:
                    self.db_manager.data_to_base('AdGroups', ad_groups.json()["result"]["AdGroups"],
                                                 campaign['Account'], strategy="Min")
                except KeyError:
                    print('При заполнении списка групп для аккаунта', account, 'компании ', campaign["_id"],
                          'яндекс не прислал необходимые данные')

    # Слова
    def get_all_keywords(self):
        for account in self.db_manager.get_accounts():
            api = API(account)
            campaigns = self.db_manager.get_campaigns_by_account(account)
            for campaign in campaigns:
                keywords = api.keywords_get(type_criteria="CampaignIds", criteria_data=[campaign["_id"]])
                try:
                    self.db_manager.data_to_base('Keywords', keywords.json()["result"]["Keywords"], campaign['Account'],
                                                 strategy="Min")
                except KeyError:
                    print('При заполнении списка слов для аккаунта', account, 'компании ', campaign["_id"],
                          'яндекс не прислал необходимые данные')

    # Ставки
    def get_all_keyword_bids(self):
        for account in self.db_manager.get_accounts():
            api = API(account)
            campaigns = self.db_manager.get_campaigns_by_account(account)
            for campaign in campaigns:
                keywords_bids = api.keywords_bids_get(type_criteria="CampaignIds", criteria_data=[campaign["_id"]])
                try:
                    self.db_manager.data_to_base('KeywordBids', keywords_bids.json()["result"]["KeywordBids"],
                                                 campaign['Account'], strategy="Min")
                except KeyError:
                    print('При заполнении списка ставок для аккаунта', account, 'компании ', campaign["_id"],
                          'яндекс не прислал необходимые данные')

    def generate_kw_info(self):
        ids = tuple(self.db_manager.db.Keywords.find().distinct("Id"))

        for kw_id in ids:
            # Заполняем конфиг
            conf = {"Strategy": "Minimal", "BidderActive": 1, "DesiredBid": 300000}
            self.db_manager.keyword_info_config_to_base(kw_id, conf)
