import time


def sliced_datetime() -> tuple:
    """
    Вернет нам день неделю и месяц назад
    :return: Time stamps ago
    """
    day = 86400
    week = 604800
    month = 2592000
    current_time = time.time()
    day_ago = current_time - day
    week_ago = current_time - week
    month_ago = current_time - month
    slices = (day_ago, week_ago, month_ago)
    return slices
