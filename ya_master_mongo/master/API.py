import json
import requests
from master.DataBase import Database
import colorama
from colorama import Fore, Back, Style


class API:
    colorama.init()
    db = Database()

    CampaignsURL = 'https://api.direct.yandex.com/json/v5/campaigns'
    AdGroupsURL = 'https://api.direct.yandex.com/json/v5/adgroups'
    KeywordsURL = 'https://api.direct.yandex.com/json/v5/keywords'
    KeywordBidsURL = 'https://api.direct.yandex.com/json/v5/keywordbids'

    def __init__(self, account_name):
        self.account_name = account_name
        self.account_data = self.db.get_token(account_name)        # Получаем логин и токен по имени акаунта
        self.token, self.login = self.account_data                 # Привязываем их к переменным
        self.headers = {"Authorization": "Bearer " + self.token,   # OAuth-токен. Использование слова Bearer обязательно
                        "Client-Login": self.login,                # Логин клиента рекламного агентства
                        "Accept-Language": "ru",                   # Язык ответных сообщений
                        }

    # ===============  Компании  ==============
    def campaigns_get(self, selection_criteria=None):
        """
        :param selection_criteria: Критерий отбора кампаний. Для получения всех кампаний должен быть пустым.
                                   "Ids":(long);
                                   "Types":"TEXT_CAMPAIGN" "MOBILE_APP_CAMPAIGN" "DYNAMIC_TEXT_CAMPAIGN";
                                   "States":"ARCHIVED" "CONVERTED" "ENDED" "OFF" "ON" "SUSPENDED";
                                   "Statuses":"ACCEPTED" "DRAFT" "MODERATION" "REJECTED";
                                   "StatusesPayment":"DISALLOWED" "ALLOWED"
        :return: Возвращает параметры кампаний, отвечающих заданным критериям.
        """

        print('Получение компаний для ', self.login)

        if selection_criteria is None:
            selection_criteria = {}

        selection_criteria["States"] = ["CONVERTED", "OFF", "ON", "SUSPENDED"]

        body = {"method": "get",
                "params": {"SelectionCriteria": selection_criteria,
                           "FieldNames": ["Id", "Name", "State"]    # Больше параметров см в "API info.txt"
                           }
                }
        json_body = json.dumps(body, ensure_ascii=False).encode('utf8')
        result = request_to_result(self.CampaignsURL, json_body, self.headers)
        return result

    # =================  Группы обьявлений  ============
    def ad_groups_get(self, type_criteria="CampaignIds", criteria_data=None):
        """
        :param type_criteria: "Ids":(long);
                              "CampaignIds": array of long;
                              "Types":"TEXT_AD_GROUP" "MOBILE_APP_AD_GROUP" "DYNAMIC_TEXT_AD_GROUP";
                              "Statuses": "ACCEPTED" | "DRAFT" | "MODERATION" | "PREACCEPTED" | "REJECTED"), ...],;
                              "ServingStatuses": "ELIGIBLE" "RARELY_SERVED";
                              "AppIconStatuses": "ACCEPTED" "MODERATION" "REJECTED";
        :param criteria_data: Список ID конкретных групп или ID копаний
        :return: Возвращает параметры групп, отвечающих заданным критериям.
        """

        print('Получение групп для ', self.login)

        if criteria_data is None:
            criteria_data = []

        body = {"method": "get",
                "params": {"SelectionCriteria": {type_criteria: criteria_data},
                           "FieldNames": ["Id", "CampaignId", "Name", "Status"]  # Больше параметров см в "API info.txt"
                           }
                }
        json_body = json.dumps(body, ensure_ascii=False).encode('utf8')
        result = request_to_result(self.AdGroupsURL, json_body, self.headers)
        return result

    # ===============  Ключевые слова ===============
    def keywords_get(self, type_criteria="CampaignIds", criteria_data=None):
        """
        :param type_criteria: "Ids":(long);
                              "AdGroupIds": array of long;
                              "CampaignIds": array of long;
                              "States": "OFF" | "ON" | "SUSPENDED";
                              "Statuses": "ACCEPTED" | "DRAFT" | "REJECTED";
                              "ServingStatuses": "ELIGIBLE" "RARELY_SERVED";
                              "ModifiedSince":  (string);
        :param criteria_data: ID или значения для критериев.
        :return: Вернет данные по ключевым словам соответствующим заданным критериям
        """

        print('Получение слова для ', self.login)

        if criteria_data is None:
            criteria_data = []

        body = {"method": "get",
                "params": {"SelectionCriteria": {type_criteria: criteria_data},
                           "FieldNames": ["Id", "AdGroupId", "CampaignId", "Keyword", "Bid", "State", "Status"]
                           # Больше параметров см в "API info.txt"
                           }
                }
        json_body = json.dumps(body, ensure_ascii=False).encode('utf8')
        result = request_to_result(self.KeywordsURL, json_body, self.headers)
        return result

    # ===============  Ставки ==================
    def keywords_bids_get(self, type_criteria="CampaignIds", criteria_data=None):
        """
        :param type_criteria: Критерий отбора групп. Для получения всех кампаний должен быть пустым
                             "KeywordIds":(long);
                             "AdGroupIds": array of long;
                             "CampaignIds": array of long;
                             "ServingStatuses": "ELIGIBLE" "RARELY_SERVED";
        :param criteria_data: ID или значения для критериев.
        :return: Возвращает ставки и приоритеты для ключевых фраз и автотаргетингов.
        """

        print('Получение ставок для ', self.login)

        if criteria_data is None:
            criteria_data = []

        body = {"method": "get",
                "params": {"SelectionCriteria": {type_criteria: criteria_data},
                           "FieldNames": ["KeywordId", "AdGroupId", "CampaignId", "ServingStatus", "StrategyPriority"],
                           "SearchFieldNames": ["Bid", "AuctionBids"],
                           # "Page": {"Limit": (long),"Offset": (long)}
                           # Структура, задающая страницу при постраничной выборке данных.
                           }
                }
        json_body = json.dumps(body, ensure_ascii=False).encode('utf8')
        result = request_to_result(self.KeywordBidsURL, json_body, self.headers)
        return result

    def keywords_bids_set(self, bids: list):
        """
        Назначает фиксированные ставки и приоритеты для ключевых фраз и автотаргетингов.
        :param bids: принимает список ставок которые надо изменить
                     ex {"KeywordBids": [{"KeywordId": (long), "SearchBid": (long)}]...}
        """

        body = {"method": "set",
                "params": {"KeywordBids": bids}
                }

        json_body = json.dumps(body, ensure_ascii=False).encode('utf8')
        try:
            result = requests.post(self.KeywordBidsURL, json_body, headers=self.headers)
            # Отладочная информация
            """
            print("Заголовки запроса: {}".format(result.request.headers))
            print("Запрос: {}".format(u(result.request.body)))
            print("Заголовки ответа: {}".format(result.headers))
            print("Ответ: {}".format(u(result.text)))
            print("\n")
            """
            # Обработка запроса
            if result.status_code != 200 or result.json().get("error", False):
                on_err(result)
            else:
                result_info(result)
                # Вывод списка кампаний
                for bid_result in result.json()["result"]["SetResults"]:
                    # result_params = ('KeywordId', 'AdGroupId', 'CampaignId', 'Warnings', 'Errors')
                    result_params = ('KeywordId', 'Warnings', 'Errors')
                    for param in result_params:
                        if bid_result.get(param):
                            print('keywords_bids_set', bid_result.get(param))

                if result.json()['result'].get('LimitedBy', False):
                    '''
                    # Если ответ содержит параметр LimitedBy, значит,  были получены не все доступные объекты.
                    # В этом случае следует выполнить дополнительные запросы для получения всех объектов.
                    # Подробное описание постраничной выборки -
                    # https://tech.yandex.ru/direct/doc/dg/best-practice/get-docpage/#page
                    '''
                    print("Получены не все доступные объекты.")

        # Обработка ошибки, если не удалось соединиться с сервером API Директа
        except ConnectionError:
            # В данном случае мы рекомендуем повторить запрос позднее
            print("Произошла ошибка соединения с сервером API.")
        # Если возникла какая-либо другая ошибка
        except Exception as ex:
            # В данном случае мы рекомендуем проанализировать действия приложения
            print("Произошла непредвиденная ошибка.")
            print(ex)


def u(x: str) -> str:
    """  UTF-8 Decoder"""
    if isinstance(x, type(b'')):
        return x.decode('utf8')
    else:
        return x


def on_err(result):
    """ Выводит сообщение при ошибке в запросе """
    print(Fore.RED + "Произошла ошибка при обращении к серверу API Директа.")
    print("Код ошибки: {}".format(result.json()["error"]["error_code"]))
    print("Описание ошибки: {}".format(u(result.json()["error"]["error_detail"])) + Style.RESET_ALL)
    # print("RequestId: {}".format(result.headers.get("RequestId", False)))


def result_info(result):
    """ При удачном получении запроса выводит инфу и проверяет все ли данные за раз получены """
    # print("RequestId: {}".format(result.headers.get("RequestId", False)))
    # print("Информация о баллах: {}".format(result.headers.get("Units", False)))
    if result.json()['result'].get('LimitedBy', False):
        # TODO все что ниже в цикл
        '''
        Если ответ содержит параметр LimitedBy, значит,  были получены не все доступные объекты.
        В этом случае следует выполнить дополнительные запросы для получения всех объектов.
        Подробное описание постраничной выборки -
        https://tech.yandex.ru/direct/doc/dg/best-practice/get-docpage/#page
        '''
        print("Получены не все доступные объекты.")


def request_to_result(url: str, body, header):
    """ Отправляем запрос и получаем ответ загнав его в переменную """
    try:
        result = requests.post(url, body, headers=header)
        # print(result.json())
        # Отладочная информация
        '''
        print("Заголовки запроса: {}".format(result.request.headers))
        print("Запрос: {}".format(u(result.request.body)))
        print("Заголовки ответа: {}".format(result.headers))
        print("Ответ: {}".format(u(result.text)))
        print("\n")
        '''
        # Обработка запроса
        if result.status_code != 200 or result.json().get("error", False):
            on_err(result)
        else:
            result_info(result)
            return result

    # Обработка ошибки, если не удалось соединиться с сервером API Директа
    except ConnectionError:
        # В данном случае мы рекомендуем повторить запрос позднее
        print("Произошла ошибка соединения с сервером API.")
    # Если возникла какая-либо другая ошибка
    except Exception as ex:
        # В данном случае мы рекомендуем проанализировать действия приложения
        print("Произошла непредвиденная ошибка.")
        print(ex)
