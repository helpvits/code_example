import statistics
from bisect import bisect, bisect_left
from math import sqrt
from DATA.config import percent_list
from master.DataBase import Database


class BidsAnalyzer:
    db = Database()

    def __init__(self):
        pass

    def keyword_bid_controller(self, yandex_kw_bid: dict) -> int:
        """
        Функция сравнения ставок и вычисления необходимой ставки каждого для слова
        :param yandex_kw_bid: Ставка одного слова (Полноценный ответ яндекса)
        :return: Новую ставку для слова
        """

        assert yandex_kw_bid["Search"]["AuctionBids"] is not None, \
            ("keyword_bid_controller Для данного слова яндекс не прислал ставки", yandex_kw_bid["KeywordId"])

        keyword_id = yandex_kw_bid['KeywordId']
        keyword_db_info = self.db.get_kw_info(keyword_id)

        keyword_bids = yandex_kw_bid["Search"]["AuctionBids"]['AuctionBidItems']
        # Отсёк лишние проценты (5 10 15)
        filtered_bids_list = list(filter(lambda traffic_vol: traffic_vol['TrafficVolume'] > 5, keyword_bids))

        for bid in filtered_bids_list:

            # Вырезана логика
            
            if "recommended" not in keyword_db_info:
                print('нет рекомендаций для слова ', keyword_id)
                pass
            else:

                # Вырезана логика

                return bid["Bid"]

        return 300000  # Вернем минималку

    def check_bids(self, new_api, campaigns_ya, account):
        """
        Сравнивает цену из яндекса с рекомендованной ценой, формирует список, заливает в яндекс
        :param new_api: Апишка
        :param campaigns_ya: Список компаний для акаунта
        :param account: акаунт
        """
        new_bids_list = []
        for Campaign in campaigns_ya.json()["result"]["Campaigns"]:

            keyword_bids_ya = new_api.keywords_bids_get(criteria_data=[Campaign["Id"]])
            self.db.data_to_base("KeywordBids", keyword_bids_ya.json()["result"]["KeywordBids"], account)

            # print(KeywordBids.json()["result"]["KeywordBids"])
            for bid in keyword_bids_ya.json()["result"]["KeywordBids"]:
                if bid["Search"]["AuctionBids"] is not None:
                    rec_bid = self.keyword_bid_controller(yandex_kw_bid=bid)
                    if rec_bid == bid["Search"]["Bid"]:
                        pass
                    elif rec_bid > 250000000:
                        pass
                    else:
                        new_bids_list.append({"KeywordId": bid["KeywordId"], "SearchBid": rec_bid})
                else:
                    print("BidsMaster Для данного слова яндекс не прислал ставки", bid["KeywordId"])

        new_api.keywords_bids_set(new_bids_list)


def get_bids_avg(bids: tuple) -> dict:
    """
    Вычисление среднего значения ставок
    :param bids: Список из данных по одному ключевому слову
    :return: {Процент:{Средняя цена, сколько средняя цена доступна, Медиана, дисперсия}, . . . }
    """

    if len(bids) == 0:
        print('Для получения среднего значения передан пустой список ставок')
        return {}

    prices_dict = get_prices_dict(bids)
    avg_dict = {}

    # Находим среднее для каждого процента
    for percent in percent_list:
        percent = str(percent)
        if str(percent) in prices_dict:
            prices_dict[percent].sort()                             # Сортируем список чтоб найти медиану и проценты
            sorted_prices_dict = prices_dict[percent]               # для удобства записи сложим в переменную
            price_dict_len = len(sorted_prices_dict)

            # Вырезана логика
            
            avg_dict[percent] = {"AvgPrice": avg_price, "Median": median, "Dispersion": dispersion,
                                 "LowAvail": low_avail, "HighAvail": high_avail}

    return avg_dict


def get_prices_dict(bids: tuple) -> dict:
    """
    Формируем словарь {процент: [список цен за него]}
    :param bids: проценты для слова
    :return: {процент: [список цен за него]}
    """
    assert len(bids) != 0, 'Для получения списка цен за проценты передар пустой список ставок'

    prices_dict = {}

    for bid in bids:
        if bid["Search"]["AuctionBids"]:
            if isinstance(bid["Search"]["AuctionBids"]["AuctionBidItems"], dict):
                for percent in bid["Search"]["AuctionBids"]["AuctionBidItems"].keys():
                    percent = str(percent)
                    percent_price = bid["Search"]["AuctionBids"]["AuctionBidItems"][percent]["Price"]
                    if percent in prices_dict:
                        prices_dict[percent].append(percent_price)
                    else:
                        prices_dict[percent] = [percent_price]

    return prices_dict


def upd_recommended_bids(keyword_id: str, db=None):
    """
    Запишет список средних ставок для kw info recommended
    {5: {Median: 1 , Dispersion: 0.1}, ...}
    :param db: подключение к базе данных
    :param keyword_id: id слова

    """

    if db is None:
        db = Database()
    info = db.get_kw_info(keyword_id)

    assert 'day' and 'week' and 'month' in info.keys(), 'Недостаточно данных для формирования рекомендованных ставок'

    recommended_bids = {}
    for i in percent_list:

        # 5 Долей дневной стате, 3 недельной, 2 месячной

        if info['day'].get(str(i)) and info['week'].get(str(i)) and info['month'].get(str(i)):
        
        # Вырезана логика
        
    db.keyword_info_to_base(recommended_bids)


