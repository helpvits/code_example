import time

import pymongo
from pymongo import MongoClient
from DATA.config import mongo_path


class Database:

    def __init__(self, db_path=mongo_path):

        self.client = MongoClient(db_path)
        assert 'version' in self.client.server_info(), "Не удалось подключиться к базе данных"
        self.db = self.client["direct"]

    def data_to_base(self, collection_name: str, result: dict, account: str, strategy="Min"):
        """
        Сохранить изменения в бд
        :param collection_name: Имя коллекции
        :param result:          Ответ от яндекса с массивом значений
        :param account:         Аккаунт которому принадлежат данные
        :param strategy:        Стратегия показов
        """

        collection = self.db[collection_name]

        # Вынесено из цикла для Keyword_bids
        current_datetime = time.strftime("%w %d %m %Y %H %M %S").split()
        current_time = time.time()

        '''
            WD = Day of week
            D = Day, M = Month , Y = Year
            h = hour, m = minute, s = sec
            TS = full time stamp
        '''
        ts = {"WD": int(current_datetime[0]), "D": int(current_datetime[1]), "M": int(current_datetime[2]),
              "Y": int(current_datetime[3]), "h": int(current_datetime[4]), "m": int(current_datetime[5]),
              "s": int(current_datetime[6]), "TS": current_time}

        for i in result:                                            # Перебираем массаив с документами
            if collection_name not in ["KeywordBids", "Keywords"]:  # Для всех кроме ставок:
                i["Strategy"] = strategy                            # Стратегия ставок можно указать на стртегию род
                i["_id"] = i["Id"]                                  # Присваиваем id обьекта как id документа монги
                i["BidderActive"] = True                            # Выключатель биддера
                i["DesiredBid"] = 0.3                               # Ставка по умолчанию

            if collection_name == "KeywordBids":  # Для ставок
                if i["Search"]["AuctionBids"] is not None:
                    i["TimeStamp"] = ts                                 # Добавим штамп времени
                    bids_dict = {}
                    for item in i["Search"]["AuctionBids"]["AuctionBidItems"]:
                        vol = item.pop("TrafficVolume")
                        bids_dict[str(vol)] = item
                    i["Search"]["AuctionBids"]["AuctionBidItems"] = bids_dict
                else:
                    print('data_to_base Нет ставок для слова:', i["KeywordId"])

            try:  # Для всех:
                i["Account"] = account                              # Принадлежность к акаунту
                collection.insert_one(i)                            # Попытка добавить данные в базу
            except pymongo.errors.DuplicateKeyError:                # Если данные уже есть:
                collection.update_one({"Id": i["Id"]},              # Обновляем данные по id
                                      {"$set": i})          # {"Id": i["Id"]} присвоение айдишника базы из айди обтекта
            except Exception as ex:
                print("Возникла не предвиденая ошибка")
                print("data_to_base", ex)

    # ======= KW Info ====
    def keyword_info_to_base(self, data: dict):
        """
        Добавить или обновить данные
        :param data: {"_id":..., "month":{5:.., 10:... }, "day": {} ...}
        """
        try:
            self.db.KeywordsInfo.insert_one(data)  # Попытка добавить данные в базу
        except pymongo.errors.DuplicateKeyError:  # Если данные уже есть:
            self.db.KeywordsInfo.update_one({"_id": data["_id"]},  # Обновляем данные по id
                                            {"$set": data})
        except Exception as ex:
            print("Возникла не предвиденая ошибка")
            print("keyword_info_to_base", ex)

    def keyword_info_config_to_base(self, keyword_id: str, conf: dict):
        """
        Дабавить/изменить конфиг слова
        :param keyword_id: id слова
        :param conf: новые данные {Strategy: ... , BidderActive: ... , DesiredBid: ... }
        :return:
        """
        result = {"_id": keyword_id, "Config": {}}
        result["Config"]["Strategy"] = conf["Strategy"]              # Стратегия ставок можно указать на стртегию род
        result["Config"]["BidderActive"] = conf["BidderActive"]         # Выключатель биддера
        result["Config"]["DesiredBid"] = conf["DesiredBid"]             # Ставка по умолчанию
        try:
            self.db.KeywordsInfo.insert_one(result)                     # Попытка добавить данные в базу
        except pymongo.errors.DuplicateKeyError:                        # Если данные уже есть:
            self.db.KeywordsInfo.update_one({"_id": result["_id"]},     # Обновляем данные по id
                                            {"$set": result})
        except Exception as ex:
            print("Возникла не предвиденая ошибка")
            print("keyword_info_config_to_base", ex)

    # Получить инфу по слову
    def get_kw_info(self, kw_id: str) -> dict:
        info = self.db.KeywordsInfo.find({"_id": kw_id})
        try:
            if info[0]:
                return info[0]
        except IndexError:
            print('get_kw_info: Инфа для слова ', kw_id, ' не найдена')

    # ======= ACCOUNT ====
    # Token & Login по аккаунту
    def get_token(self, account: str) -> list:
        account = self.db.Accounts.find({"Account": account})
        return [account[0]["Token"], account[0]["Login"]]

    def get_accounts(self) -> tuple:
        return tuple(self.db.Accounts.find().distinct("Account"))

    # ======= CAMPAIGNS ======
    # получить компании аккаунта которые не заархивированы
    def get_campaigns_by_account(self, account):
        campaigns = self.db.Campaigns.find(
                {'$and': [{"Account": account}, {'State': {'$not': {'$eq': 'ARCHIVED'}}}]})
        return campaigns

    # Получить данные определенной компании
    def get_campaign_by_id(self, campaign_id: str) -> tuple:
        campaign = self.db.Campaigns.find({"Id": campaign_id})
        return tuple(campaign[0])

    # ========= GROUPS =======
    # Получить данные группы
    def get_group_by_id(self, group_id: str) -> dict:
        group = self.db.AdGroups.find({"Id": group_id})
        return group[0]

    # ======= KEYWORDS =======
    def keywords_dump(self) -> tuple:
        """
        Получить все ключевые слова
        """
        keywords = self.db.Keywords.find()
        return tuple(keywords)

    def get_keyword_by_id(self, keyword_id: str) -> dict:
        """
        Данные ключевого слова по id
        """
        keyword = self.db.Keywords.find({"Id": keyword_id})
        return keyword[0]



    # ====== KEYWORDS BIDS ======
    # TODO Not Used
    def get_bids(self) -> dict:
        """
        Получаем все ставки для всех слов
        """
        bids = self.db.KeywordBids.find()
        return bids

    def get_bids_by_id(self, keyword_id: str, timestamp=None) -> tuple:
        """
        Получить ставку для определенного слова
        """
        if timestamp:                                                   # Срез веремени после которого выбирать ставки
            bids = tuple(self.db.KeywordBids.find({'KeywordId': keyword_id, 'TimeStamp.TS': {'$gt': timestamp}},
                                                  {'_id': False}))
        else:                                                           # Выбрать все ставки
            bids = tuple(self.db.KeywordBids.find({"KeywordId": keyword_id}, {'_id': False}))
        return bids

    def clear_keyword_bids(self, timestamp):
        """
        Очищает базу от данных старше одного месяца
        """
        self.db.db.KeywordBids.delete_many({"TimeStamp.TS": {'$lt': int(timestamp)}})