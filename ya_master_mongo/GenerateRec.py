import multiprocessing
from datetime import datetime
from master.BidsAnalyzer import get_bids_avg, upd_recommended_bids
from master.DataBase import Database
from master.Etc import sliced_datetime

day_ago, week_ago, month_ago = sliced_datetime()
db = Database()


def generate(word_id: str):
    info = {"_id": word_id, "month": {}, "week": {}, "day": {}}

    # Генерим данные в инфо слов за месяц
    monthly_bids = db.get_bids_by_id(word_id, month_ago)

    if len(monthly_bids) > 0:
        month_data = get_bids_avg(monthly_bids)
        info["month"] = month_data

        # Генерим данные в инфо слов за неделю
        weekly_bids = data_slicer(monthly_bids, week_ago)

        if len(weekly_bids) > 0:
            week_data = get_bids_avg(weekly_bids)
            info["week"] = week_data

            # Генерим данные в инфо слов за день
            daily_bids = data_slicer(monthly_bids, day_ago)

            if len(daily_bids) > 0:
                day_data = get_bids_avg(daily_bids)
                info["day"] = day_data
            else:
                print('generate не найдены данные для слова за день')

        else:
            print('generate не найдены данные для слова за неделю')

    else:
        print('generate не найдены данные для слова за месяц')

    db.keyword_info_to_base(info)
    upd_recommended_bids(word_id, db)

    return "1"  # Нужно для асинхронного выполнения


def data_slicer(data: tuple, time_stamp: int,) -> tuple:
    """
    оставляет только те данные которые появились раньше time_stamp
    :param time_stamp: Время после которого данные отсекаются
    :param data: данные для фильтрации
    :return:
    """
    return tuple(filter(lambda n: n["TimeStamp"]["TS"] < time_stamp, data))


if __name__ == '__main__':
    start = datetime.now()
    print(start)

    ids = db.db.Keywords.find().distinct("Id")

    # # 1 поток для отладки профайлом
    # for kw_id in ids:
    #     generate(kw_id)

    pool = multiprocessing.Pool(processes=30)
    r = pool.map_async(generate, ids)
    r.wait()

    print(datetime.now() - start)



