db = db.getSiblingDB('direct');
db.createUser(
  {
    user: '************',
    pwd: '*********************',
    roles: [{ role: 'readWrite', db: 'direct' }],
  },
);
db.createCollection('Accounts');
db.createCollection('Campaigns');
db.createCollection('AdGroups');
db.createCollection('Keywords');
db.createCollection('KeywordBids');
db.createCollection('KeywordsInfo');