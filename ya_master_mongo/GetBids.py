from master import API
from master.DataBase import Database

db = Database()
accounts = db.get_accounts()

for account in accounts:
    new_api = API.API(account)
    Campaigns = new_api.campaigns_get()
    if Campaigns:
        for Campaign in Campaigns.json()["result"]["Campaigns"]:
            KeywordBids = new_api.keywords_bids_get(criteria_data=[Campaign["Id"]])
            db.data_to_base("KeywordBids", KeywordBids.json()["result"]["KeywordBids"], account)