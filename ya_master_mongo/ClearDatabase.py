from master.DataBase import Database
from master.Etc import sliced_datetime

db = Database()
day_ago, week_ago, month_ago = sliced_datetime()
db.clear_keyword_bids(day_ago)
