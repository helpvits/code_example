from master import API
from master.DataBase import Database
from master.BidsAnalyzer import BidsAnalyzer

db = Database()
accounts = db.get_accounts()
bids_analyzer = BidsAnalyzer()


for account in accounts:
    new_api = API.API(account)
    campaigns_ya = new_api.campaigns_get()
    if campaigns_ya is None:
        print('Для данного акаунта не получены данные от яндекса: ', account)
        pass
    else:
        bids_analyzer.check_bids(new_api, campaigns_ya, account)
