from master import Install

installer = Install.InstallData()

installer.install()
installer.get_all_campaigns()
installer.get_all_groups()
installer.get_all_keywords()
installer.get_all_keyword_bids()
installer.generate_kw_info()
